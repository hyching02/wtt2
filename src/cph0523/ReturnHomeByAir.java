package cph0523;

public class ReturnHomeByAir extends ReturnHome{
	
	protected void buyTicket() {
		System.out.println("Buy airplane ticket.");
	}
	
	protected void transport() {
		System.out.println("Take airplane to Taipei main station.");
	}
}