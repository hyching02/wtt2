package cph0523;

public abstract class ReturnHome {
	public void meetParent() {
		buyTicket();
		transport();
		localTransport();
	}
	
	
	protected abstract void buyTicket();
	
	protected abstract void transport();
	
	protected final void localTransport() {
		System.out.println("Take MRT to Taipei Zoo station.");
		System.out.println("Take bus to home.");
		System.out.println("");
		
	}
}

