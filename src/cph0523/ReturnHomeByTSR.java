package cph0523;

public class ReturnHomeByTSR extends ReturnHome{
	
	protected void buyTicket() {
		System.out.println("Buy TSR ticket.");
	}
	
	protected void transport() {
		System.out.println("Take TSR to Taipei main station.");
	}
}
