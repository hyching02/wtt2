package cph0523;

public class ReturnHomeByCoach extends ReturnHome{
	
	protected void buyTicket() {
		System.out.println("Buy coach ticket.");
	}
	
	protected void transport() {
		System.out.println("Take coach to Taipei main station.");
	}
}