package cph0523;

public class main {

	public static void main(String[] args) {

		ReturnHome tsr = new ReturnHomeByTSR();
		ReturnHome coach = new ReturnHomeByCoach();
		ReturnHome air = new ReturnHomeByAir();
		
		System.out.println("Students go home on by one......");
		System.out.println();
		
		//1
		System.out.println("Andy goes home at first.");
		air.meetParent();
		
		//2
		System.out.println("Secondly, Jessie goes home.");
		tsr.meetParent();
		
		//3
		System.out.println("Thirdly, Coco goes home.");
		coach.meetParent();
		
	}

}
